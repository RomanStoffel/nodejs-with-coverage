# NodeJS project with coverage

To run tests and get Sonar analysis with coverage, execute 

```
npm run test
npm install -g sonarqube-scanner
sonar-scanner  -Dsonar.projectKey=js-with-coverage:feature-some-bugs
```
